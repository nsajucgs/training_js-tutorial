import logo from './logo.svg';
import './App.css';
import ImageRender from './ImageRender';

function App() {
  return (
    <div>
      <ImageRender />
    </div>
  );
}

export default App;
