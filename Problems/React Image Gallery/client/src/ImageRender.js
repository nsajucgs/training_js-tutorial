import React, { Component } from "react";
import axios from "axios";
const PAGE_LIMIT = 20;

class ImageRender extends Component{
    constructor(){
        super();
        this.state = {
            imagesList : [],
            currentPage: 1,
            totalPages: 0,
            currentPageResults: []
            // limit : "20",
            // TotalCount : "100",
            // page : (TotalCount/limit)
        }
    }

    componentDidMount(){
       this.getImages();   
    }

    async getImages() {
        const response = await axios.get("http://localhost:5000/api/v1/image-list");
        //console.log(response)
        let totalCount = response.data.length
        let totalPages = Math.floor(totalCount/PAGE_LIMIT);
        await this.setState({
            imagesList: response.data,
            totalPages: totalPages
        })
        this.getPageResults();     
    }

    getPageResults() {
        const { currentPage, imagesList} = this.state;
        const startIndex = (currentPage - 1) * PAGE_LIMIT
        const endIndex = currentPage * PAGE_LIMIT
        const results = imagesList.slice(startIndex , endIndex);
        this.setState({
            currentPageResults: results
        })
    }

    onPageChange=(actionType)=> {
        if(actionType=="Previous"){
            //console.log(this.state.currentPage)
            this.setState({
                currentPage: this.state.currentPage - 1
            }, () => this.getPageResults())
        }else{
            // console.log(this.state.currentPage)
            this.setState({
                currentPage: this.state.currentPage + 1
            }, () => this.getPageResults())
        }
   }

    onPageNoClick = (pageNo) => {
        this.setState({
            currentPage: pageNo
        }, () => this.getPageResults())
   }

    
    render(){
        console.log(this.state.image)   
        const divStyle = {
            height : "100px",
            width : "100px"
        }

        const pageNumbers = {
            color: 'black',
            padding: '8px 16px',
            fontSize : '20px',
        }

        const pagination ={
            paddingLeft : '750px',
            paddingTop : '100px'
        }

        let pageLinks = [];
        for(let idx = 1; idx <= this.state.totalPages; idx++){
            const activeStyle = idx === this.state.currentPage ? { backgroundColor: "blue"} : {};
            pageLinks.push(<a style={{...activeStyle, ...pageNumbers}} href="#" onClick={() => this.onPageNoClick(idx)}>{idx}</a>)
        }

        const images = this.state.currentPageResults.map(function(item,index){
            return (<img style={divStyle} key={index} src ={item.url}></img>)
        })
        const previousBtn = this.state.currentPage !== 1 && (<button  onClick={() => this.onPageChange("Previous")}>Previous</button>);
        const nextBtn = this.state.currentPage !== this.state.totalPages && (<button  onClick={() => this.onPageChange("nex")}>Next</button>);

        return(
            <div>
                <div>{`Page ${this.state.currentPage} of ${this.state.totalPages}`}</div>
                <div>
                    {images}
                </div>
                <div style={pagination}>
                    {previousBtn}
                    {pageLinks}
                    {nextBtn}
                </div> 
            </div>
            
        )
    }
}

export default ImageRender