import React from "react";

function Tabledata(props){
    const rowsArr = new Array(Number(props.rows)).fill('1') //[1, 1]
    const columnsArr = new Array(Number(props.columns)).fill('1') // [1, 1, 1]
    //console.log(rowsArr,columnsArr)

    const borderStyle={border: "1px solid black",
        borderCollapse: "collapse"}

    const tableBody = (
        <tbody>
        {rowsArr.map(function(val, indexrow) {
            return (
                <tr key={indexrow}>
                    {columnsArr.map(function(val, indexcolumn){
                        var x = (indexrow * indexcolumn) % 256
                        
                        return (
                            <td style={{...borderStyle, backgroundColor: `rgb(${x}, ${x}, ${x})`}} key={indexcolumn}>{`${indexrow}${indexcolumn}`}</td>
                        )
                    })}
                </tr>
            )
        })}
        </tbody>
    );

    return(
        <table style={borderStyle}>
            {tableBody}
        </table>
    )
}

export default Tabledata


//{...borderStyle,  backkgroundColor: "rgb(x, x, x)"}