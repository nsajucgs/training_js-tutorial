
import React from 'react';
import './App.css';
import Tabledata from './Tabledata';

class App extends React.Component{

   constructor(props){
     super(props)
     this.state={
       rows:"",
       columns:""
     }
    }

  onRowChange=(e)=>{
      //console.log(e)
      this.setState({rows:e.target.value})

  }


  onColumnChange=(e)=>{
    this.setState({columns:e.target.value})
  }

   render(){
     return(
       <div>
         <form>
         <div>
         <label htmlFor='rows'>Enter the Rows</label>
         <input onChange={this.onRowChange} id="rows" type="number"></input>
         </div>
         <div>
         <label htmlFor='columns'>Enter the Columns</label> 
         <input onChange={this.onColumnChange} id="colomns" type="number"></input>
         </div>
       </form>
       <Tabledata rows={this.state.rows} columns={this.state.columns}/>
      </div>
    
     ) 
  }
}

export default App