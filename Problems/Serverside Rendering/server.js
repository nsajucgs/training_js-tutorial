const express = require ("express")
const app =   express()

function getImages(count=100){
    var dimensions = ['200' , '250' , '300' , '350' , '400']

    getRandomInt=()=>{
        return Math.floor (Math.random() * dimensions.length)
    }

    var imageArray = [];

    for(var i = 0 ; i <= count ; i++){
        const width = dimensions[getRandomInt()]
        const height = dimensions[getRandomInt()]

        imageArray.push({
            "url" : `https://loremflickr.com/${width}/${height}`,
            "width" : width,
            "height" : height,
            "position" : i
        })
    }

    return new Promise(function(resolve , reject){
        //console.log(imageArray)
        resolve(imageArray)
    })
}

app.get('/' , (req, res, next)=>{
    getImages().then(function(data){
        //console.log(getImages())
        const page = req.query.page
        const limit = req.query.limit

        const starIndex = (page - 1) * limit
        const endIndex = page * limit

        const result = data.slice(starIndex , endIndex)

        var props = {
            Page : page,
            Size : limit,
            Data : result
        }

        res.json(props)
    })

})

app.listen(5000)