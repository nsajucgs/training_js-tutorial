var express = require('express');
var router = express.Router();


function getImages() {
  var dimensions = ["200" , "250" , "300" , "350" , "400"]
   

  getRandomInt=()=>{
    return Math.floor(Math.random() * dimensions.length) // 0,1,2,3,4
  }

  var imageArr = []; //getting array of 20 objects
  for(var i = 0 ; i <=100 ; i++ ){
    const width = dimensions[getRandomInt()] // 1 
    const height = dimensions[getRandomInt()] // 3
    
    imageArr.push({
      "url" : `https://loremflickr.com/${width}/${height}`,
      "width" : width,
      "height" : height,
      "position" : i
    })
  }

  return new Promise(function(resolve, reject) {
    resolve(imageArr)
  })
}


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express Project' });
});

router.get('/api/v1/image-list', function(req, res, next) {
  getImages().then(function(data) {
    res.render('imageGallery' , {imageArray : data})
    
  })
})

router.get('/jquery' , function(req,res,next){
 res.render('imageGalleryJquery')
})

router.get('/gallery', function(req, res, next) {
  getImages().then(function(data) {
    const page = req.query.page
    const limit = req.query.limit

    const startIndex = (page - 1) * limit
    const endIndex = page * limit
   
    const result = data.slice(startIndex , endIndex)

    var props = {
      Page : page,
      Size : limit,
      Data : result,
      
    }

    //res.json(props)
    
    res.render('pagination' , {imageArray : props.Data, pageNumber : props.Page, sizeLimit : props.Size })
   
  })
})



module.exports = router;
