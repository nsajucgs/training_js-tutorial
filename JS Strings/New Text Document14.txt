<!DOCTYPE html>
<html>
<body>
<h2>JavaScript Strings</h2>
<h3>The match() Method</h3>
<p>match() searches for a match in a string.</p>
<p = id = "demo"></p>
<p = id = "demo2"></p>
<p = id = "demo3"></p>
<script>
let x = "Twinkle twinkle little star"
document.getElementById('demo').innerHTML = x.match(/twi/) //searching for a match in string
document.getElementById('demo2').innerHTML = x.match(/twi/g)
document.getElementById('demo3').innerHTML = x.match(/twi/gi)// global search with case sensitive


</script>



</body>
</html>