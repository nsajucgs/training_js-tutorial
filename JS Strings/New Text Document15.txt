<!DOCTYPE html>
<html>
<body>

<h1>JavaScript Objects</h1>
<h2>The prototype Property</h2>

<p id="demo"></p>
<p id="demo2"></p>

<script>

function myfunction(Brand,Price,Color){
this.carbrand = Brand;
this.carprice = Price;
this.carcolor = Color;

}
myfunction.prototype.carseating = 5

let car1 = new myfunction("Audi","10K","Red")
let car2 = new myfunction("BMW","15K","Blue")
document.getElementById('demo').innerHTML = car2.carbrand
document.getElementById('demo2').innerHTML = car2.carseating //adding property 'seating' to all objects


</script>

</body>
</html>