<!DOCTYPE html>
<html>
<body>

<h1>JavaScript Strings</h1>
<h2>The charCodeAt() Method</h2>

<p>charCodeAt() returns the Unicode of the character at a specified position in a string.</p>
<p>Get the Unicode of the last character:</p>
<p id = "demo"></p>

<script>
let x = "Nithin Saju";

document.getElementById('demo').innerHTML = x.charCodeAt(x.length -1)


</script>



</body>
</html>